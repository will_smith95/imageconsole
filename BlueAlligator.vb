﻿Imports System.Xml
Imports System.Net
Imports System.IO
Imports System.Text
Imports System.Threading
Imports System.Net.Mail

Module BlueAlligator

    Dim theFile As String
    Dim fileLength As String
    Dim filename As String
    Dim errors As String

    Sub Main()

        Try
            the_csv_files()
            Process.Start("\\ptlstore\public\programs\read-only.bat")
        Catch ex As Exception

        End Try

        Dim Directory As New IO.DirectoryInfo("\\ptlstore\public\Photos & Images\BlueAlligatorImages")
        Dim allFiles As IO.FileInfo() = Directory.GetFiles()
        Dim singleFile As IO.FileInfo
        Dim max As Integer
        Dim i As Integer
        i = 1
        max = allFiles.Count
        If max = 0 Then
            max = 1
        End If

        For Each singleFile In allFiles
            Try
                'Label2.Text = ((i / max) * 100).ToString + "%"
                filename = singleFile.ToString

                If filename.Trim.Contains("-1") Then

                    filename = filename.Replace("-1", "")
                End If

                If filename <> "Thumbs.db" Then

                    If filename.Contains("-1") Then

                        filename = filename.Replace("-1", "")
                    End If

                    fileLength = FileTLength(singleFile.FullName)
                    theFile = ConvertFileToBase64(singleFile.FullName)
                    readAndUpload(theFile, fileLength, filename)
                    If File.Exists("\\ptlstore\public\Photos & Images\BlueAlligatorImages\Pending\" + singleFile.ToString) Then
                        Try
                            singleFile.Delete()
                        Catch ex As Exception
                        End Try
                    Else
                        Try
                            singleFile.MoveTo("\\ptlstore\public\Photos & Images\BlueAlligatorImages\Pending\" + singleFile.ToString)
                        Catch ex As Exception
                        End Try

                        'singleFile.Delete()
                    End If
                    i = i + 1
                End If
            Catch ex As Exception

            End Try
        Next

        Try

            If errors <> "" Then
                Try
                    Dim SmtpServer As New SmtpClient()
                    SmtpServer.Credentials = New Net.NetworkCredential("ptl.smtp.rt@gmail.com", "74242852")
                    SmtpServer.Port = 587
                    SmtpServer.Host = "smtp.gmail.com"
                    SmtpServer.EnableSsl = True
                    Dim omail As New MailMessage()
                    omail.From = New MailAddress("VB.APPLICATION@pricecheck.uk.com", "BLUE ALLIGATOR UPLOADER", System.Text.Encoding.UTF8)
                    omail.Subject = "Blue Alligator Completed with Errors"
                    omail.To.Add("ITSupport@pricecheck.uk.com")
                    omail.Body = "The uploader completed with errors:" & vbCr & vbCr + errors
                    SmtpServer.Send(omail) ', Nothing)
                    omail.Dispose()
                Catch ex As Exception

                End Try
            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub the_csv_files()

        Dim Directory As New IO.DirectoryInfo("\\ptlstore\public\Photos & images\BlueAlligator")
        Dim allFiles As IO.FileInfo() = Directory.GetFiles()
        Dim singleFile As IO.FileInfo
        Dim max As Integer
        Dim i As Integer
        i = 1
        max = allFiles.Count
        If max = 0 Then
            max = 1
        End If

        For Each singleFile In allFiles

            Try
                filename = singleFile.ToString
                fileLength = FileTLength(singleFile.FullName)
                theFile = ConvertFileToBase64(singleFile.FullName)
                readAndUpload(theFile, fileLength, filename)
            Catch ex As Exception

            End Try
            'Label2.Text = ((i / max) * 100).ToString + "%"



            'RichTextBox1.Text = RichTextBox1.Text + _
            'filename(+vbCr + fileLength + theFile + vbCr)




            i = i + 1
        Next
        finalprocess()

    End Sub

    Sub finalprocess()

        Dim postData As String = "<baccloud>" & vbCr + "   <licence>6212050001</licence>" & vbCr + _
    "   <activation>ACEE-7AE1-FA97-D352</activation>" & vbCr + "</baccloud>"

        Dim tempCookies As New CookieContainer
        Dim encoding As New UTF8Encoding
        Dim byteData As Byte() = encoding.GetBytes(postData)

        Dim postReq As HttpWebRequest = _
        DirectCast(WebRequest.Create("http://www.bacapps.co.uk/baccloud/integrationservice.asmx/processfiles"), HttpWebRequest)
        postReq.Method = "POST"
        postReq.KeepAlive = True
        postReq.CookieContainer = tempCookies
        postReq.ContentType = "application/x-www-form-urlencoded"
        'postReq.Referer = "http://forums.zybez.net/index.php?app=core&module=global&section=login&do=process"
        postReq.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.1; ru; rv:1.9.2.3) Gecko/20100401 Firefox/4.0 (.NET CLR 3.5.30729)"
        postReq.ContentLength = postData.Length

        Dim postreqstream As Stream = postReq.GetRequestStream()


        postreqstream.Write(byteData, 0, byteData.Length)
        postreqstream.Close()
        Dim postresponse As HttpWebResponse
        Try
            postresponse = DirectCast(postReq.GetResponse(), HttpWebResponse)

            tempCookies.Add(postresponse.Cookies)
            'logincookie = tempCookies



            Dim postreqreader As New StreamReader(postresponse.GetResponseStream())
            postresponse.Close()

        Catch ex As Exception

        End Try
    End Sub

    Sub readAndUpload(theFile As String, fileLength As String, filename As String)
        Try
            Dim theXML As String
            theXML = "<baccloud>" + vbCr + _
                     "    <licence>6212050001</licence>" + vbCr + _
                     "    <activation>ACEE-7AE1-FA97-D352</activation>" + vbCr + _
                     "    <filename>" + filename + "</filename>" + vbCr + _
                     "    <currentchunk>0</currentchunk>" + vbCr + _
                     "    <maxbytes>0</maxbytes>" + vbCr + _
                     "    <filelength>" + fileLength.Trim + "</filelength>" + vbCr + _
                     "    <filedata>" + theFile + "</filedata>" + vbCr + _
                     "    <backofficeref></backofficeref>" + vbCr + _
                     "</baccloud>"



            Dim tempCookies As New CookieContainer
            Dim encoding As New UTF8Encoding
            Dim byteData As Byte() = encoding.GetBytes(theXML)

            Dim postReq As HttpWebRequest = _
            DirectCast(WebRequest.Create("http://www.bacapps.co.uk/baccloud/integrationservice.asmx/uploadfile"), HttpWebRequest)
            postReq.Method = "POST"
            postReq.KeepAlive = True
            postReq.CookieContainer = tempCookies
            postReq.ContentType = "application/x-www-form-urlencoded"

            postReq.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.1; ru; rv:1.9.2.3) Gecko/20100401 Firefox/4.0 (.NET CLR 3.5.30729)"
            postReq.Timeout = 500000

            postReq.ContentLength = theXML.Length

            Dim postreqstream As Stream = postReq.GetRequestStream()
            postreqstream.Write(byteData, 0, byteData.Length)
            postreqstream.Close()



            Dim postresponseupload As HttpWebResponse


            postresponseupload = DirectCast(postReq.GetResponse(), HttpWebResponse)
            Dim postreqreader As New StreamReader(postresponseupload.GetResponseStream())

        Catch ex As Exception

            errors = errors + filename + " filelength:" + fileLength & vbCr
        End Try
    End Sub

    Public Function ConvertFileToBase64(ByVal fileName As String) As String
        Dim ReturnValue As String = ""
        If My.Computer.FileSystem.FileExists(fileName) Then
            Using BinaryFile As FileStream = New FileStream(fileName, FileMode.Open)
                Dim BinRead As BinaryReader = New BinaryReader(BinaryFile)
                Dim BinBytes As Byte() = BinRead.ReadBytes(CInt(BinaryFile.Length))
                ReturnValue = Convert.ToBase64String(BinBytes)
                BinaryFile.Close()
            End Using
        End If
        Return ReturnValue
    End Function

    Public Function FileTLength(ByVal fileName As String) As String
        Dim ReturnValue As String = ""
        Try
            If My.Computer.FileSystem.FileExists(fileName) Then
                Using BinaryFile As FileStream = New FileStream(fileName, FileMode.Open)
                    Dim BinRead As BinaryReader = New BinaryReader(BinaryFile)
                    Dim BinBytes As Byte() = BinRead.ReadBytes(CInt(BinaryFile.Length))
                    ReturnValue = BinaryFile.Length.ToString
                    BinaryFile.Close()
                End Using
            End If
        Catch ex As Exception
            
        End Try

        Return ReturnValue
    End Function
End Module
