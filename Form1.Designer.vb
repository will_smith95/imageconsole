﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.RadSplitContainer1 = New Telerik.WinControls.UI.RadSplitContainer()
        Me.SplitPanel1 = New Telerik.WinControls.UI.SplitPanel()
        Me.btn_help = New Telerik.WinControls.UI.RadButton()
        Me.btn_picCheck = New Telerik.WinControls.UI.RadButton()
        Me.btn_upload = New Telerik.WinControls.UI.RadButton()
        Me.img_uploader = New System.Windows.Forms.PictureBox()
        Me.bgw_uploadFiles = New System.ComponentModel.BackgroundWorker()
        Me.bgw_finishUploading = New System.ComponentModel.BackgroundWorker()
        Me.bgw_cloudinaryLoader = New System.ComponentModel.BackgroundWorker()
        Me.bgw_blueAlligator = New System.ComponentModel.BackgroundWorker()
        Me.bgw_blueAlligatorCustomers = New System.ComponentModel.BackgroundWorker()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.RadSplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_help, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_picCheck, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_upload, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_uploader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RadSplitContainer1
        '
        Me.RadSplitContainer1.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.RadSplitContainer1.Controls.Add(Me.SplitPanel1)
        Me.RadSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RadSplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.RadSplitContainer1.Name = "RadSplitContainer1"
        Me.RadSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        '
        '
        Me.RadSplitContainer1.RootElement.AccessibleDescription = Nothing
        Me.RadSplitContainer1.RootElement.AccessibleName = Nothing
        Me.RadSplitContainer1.RootElement.ControlBounds = New System.Drawing.Rectangle(0, 0, 200, 200)
        Me.RadSplitContainer1.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.RadSplitContainer1.Size = New System.Drawing.Size(1034, 627)
        Me.RadSplitContainer1.SplitterWidth = 4
        Me.RadSplitContainer1.TabIndex = 0
        Me.RadSplitContainer1.TabStop = False
        Me.RadSplitContainer1.Text = "RadSplitContainer1"
        '
        'SplitPanel1
        '
        Me.SplitPanel1.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.SplitPanel1.BackgroundImage = Global.ImageConsole.My.Resources.Resources.ImageConsoleBG
        Me.SplitPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.SplitPanel1.Controls.Add(Me.btn_help)
        Me.SplitPanel1.Controls.Add(Me.btn_picCheck)
        Me.SplitPanel1.Controls.Add(Me.btn_upload)
        Me.SplitPanel1.Location = New System.Drawing.Point(0, 0)
        Me.SplitPanel1.Name = "SplitPanel1"
        '
        '
        '
        Me.SplitPanel1.RootElement.AccessibleDescription = Nothing
        Me.SplitPanel1.RootElement.AccessibleName = Nothing
        Me.SplitPanel1.RootElement.ControlBounds = New System.Drawing.Rectangle(0, 0, 200, 200)
        Me.SplitPanel1.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.SplitPanel1.Size = New System.Drawing.Size(1034, 627)
        Me.SplitPanel1.SizeInfo.AutoSizeScale = New System.Drawing.SizeF(0!, -0.2253086!)
        Me.SplitPanel1.SizeInfo.SplitterCorrection = New System.Drawing.Size(0, -146)
        Me.SplitPanel1.TabIndex = 0
        Me.SplitPanel1.TabStop = False
        Me.SplitPanel1.Text = "SplitPanel1"
        '
        'btn_help
        '
        Me.btn_help.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_help.Font = New System.Drawing.Font("Segoe UI", 20.0!)
        Me.btn_help.Location = New System.Drawing.Point(980, 570)
        Me.btn_help.Name = "btn_help"
        '
        '
        '
        Me.btn_help.RootElement.AccessibleDescription = Nothing
        Me.btn_help.RootElement.AccessibleName = Nothing
        Me.btn_help.RootElement.ControlBounds = New System.Drawing.Rectangle(980, 570, 110, 24)
        Me.btn_help.Size = New System.Drawing.Size(42, 45)
        Me.btn_help.TabIndex = 2
        Me.btn_help.Text = "?"
        '
        'btn_picCheck
        '
        Me.btn_picCheck.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_picCheck.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.btn_picCheck.Location = New System.Drawing.Point(793, 236)
        Me.btn_picCheck.Name = "btn_picCheck"
        '
        '
        '
        Me.btn_picCheck.RootElement.AccessibleDescription = Nothing
        Me.btn_picCheck.RootElement.AccessibleName = Nothing
        Me.btn_picCheck.RootElement.ControlBounds = New System.Drawing.Rectangle(793, 236, 110, 24)
        Me.btn_picCheck.Size = New System.Drawing.Size(150, 45)
        Me.btn_picCheck.TabIndex = 1
        Me.btn_picCheck.Text = "Picture Checker"
        '
        'btn_upload
        '
        Me.btn_upload.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_upload.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.btn_upload.Location = New System.Drawing.Point(599, 236)
        Me.btn_upload.Name = "btn_upload"
        '
        '
        '
        Me.btn_upload.RootElement.AccessibleDescription = Nothing
        Me.btn_upload.RootElement.AccessibleName = Nothing
        Me.btn_upload.RootElement.ControlBounds = New System.Drawing.Rectangle(599, 236, 110, 24)
        Me.btn_upload.Size = New System.Drawing.Size(150, 45)
        Me.btn_upload.TabIndex = 0
        Me.btn_upload.Text = "Upload New Images"
        '
        'img_uploader
        '
        Me.img_uploader.BackColor = System.Drawing.Color.Transparent
        Me.img_uploader.Image = CType(resources.GetObject("img_uploader.Image"), System.Drawing.Image)
        Me.img_uploader.Location = New System.Drawing.Point(727, 304)
        Me.img_uploader.Name = "img_uploader"
        Me.img_uploader.Size = New System.Drawing.Size(111, 103)
        Me.img_uploader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.img_uploader.TabIndex = 3
        Me.img_uploader.TabStop = False
        Me.img_uploader.Visible = False
        '
        'bgw_uploadFiles
        '
        '
        'bgw_finishUploading
        '
        '
        'bgw_cloudinaryLoader
        '
        '
        'bgw_blueAlligator
        '
        '
        'bgw_blueAlligatorCustomers
        '
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(13, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "V1.3"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1034, 627)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.img_uploader)
        Me.Controls.Add(Me.RadSplitContainer1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Image Console"
        CType(Me.RadSplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_help, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_picCheck, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_upload, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_uploader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bgw_uploadFiles As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgw_finishUploading As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgw_cloudinaryLoader As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgw_blueAlligator As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgw_blueAlligatorCustomers As System.ComponentModel.BackgroundWorker
    Friend WithEvents img_uploader As System.Windows.Forms.PictureBox
    Private WithEvents RadSplitContainer1 As Telerik.WinControls.UI.RadSplitContainer
    Private WithEvents SplitPanel1 As Telerik.WinControls.UI.SplitPanel
    Private WithEvents btn_picCheck As Telerik.WinControls.UI.RadButton
    Private WithEvents btn_upload As Telerik.WinControls.UI.RadButton
    Private WithEvents btn_help As Telerik.WinControls.UI.RadButton
    Friend WithEvents Label1 As Label
End Class
