﻿Imports System.IO
Imports System.Data.SqlClient

Public Class Form1

    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        initialise()
    End Sub

    Private Sub initialise()

        '------------------------- Handlers -----------------------
        AddHandler btn_picCheck.Click, AddressOf OpenPictureChecker
        AddHandler btn_upload.Click, AddressOf UploadImages
        AddHandler Me.KeyDown, AddressOf RefreshCheck
        AddHandler btn_help.Click, AddressOf Helper
        '------------------------- Methods ------------------------

        CheckNewImages()
        Me.KeyPreview = True
    End Sub

    Dim path As String = "\\PTLSTORE\Public\Photos & images\PHOTOBOX\PRESENTATION PENDING\"
    Dim destination As String = "\\PTLSTORE\Public\Photos & images\PHOTOBOX\PRESENTATION COMPLETED\"
    Dim cloudinaryPath As String = "\\PTLSTORE\Public\Photos & images\PHOTOBOX\CLOUDINARY PENDING\"
    Dim blueAlligatorPath As String = "\\PTLSTORE\Public\Photos & images\BlueAlligatorImages\"
    Dim blueAlligatorPending As String = "\\PTLSTORE\Public\Photos & images\BlueAlligatorImages\Pending\"
    Dim productList As New ArrayList
    Dim fileList As New ArrayList
    Dim failedImages As New ArrayList
    Dim errorMsg As String = ""

    Public Sub BlueAlligatorUploader()

        If bgw_blueAlligator.IsBusy Then

            MsgBox("Uploading To Blue Alligator")
        Else

            bgw_blueAlligator.RunWorkerAsync()
        End If
    End Sub

    Public Sub BlueAlligatorCustomerUploader()

        If bgw_blueAlligatorCustomers.IsBusy Then

            MsgBox("Uploading To Blue Alligator Customers")
        Else

            bgw_blueAlligatorCustomers.RunWorkerAsync()
        End If
    End Sub

    Public Sub CloudinaryUploader()

        If bgw_cloudinaryLoader.IsBusy Then

            MsgBox("Uploading To Cloudinary")
        Else

            bgw_cloudinaryLoader.RunWorkerAsync()
        End If
    End Sub

    Public Sub CheckNewImages()

        If Directory.GetFiles(path).Count = 0 And Directory.GetFiles(cloudinaryPath).Count = 0 And Directory.GetFiles(blueAlligatorPath).Count = 0 And Directory.GetFiles(blueAlligatorPending).Count = 0 Then

            btn_upload.Enabled = False
            btn_picCheck.Enabled = True

        ElseIf Directory.GetFiles(path).Count = 1 And Directory.GetFiles(cloudinaryPath).Count = 1 And Directory.GetFiles(blueAlligatorPath).Count = 1 And Directory.GetFiles(blueAlligatorPending).Count = 1 Then

            If File.Exists(path + "Thumbs.db") And File.Exists(cloudinaryPath + "Thumbs.db") Then

                btn_upload.Enabled = False
                btn_picCheck.Enabled = True
            End If
        Else

            btn_upload.Enabled = True
            btn_picCheck.Enabled = True

            Dim imgFiles() As String = Directory.GetFiles(path)
            failedImages.Clear()

            For Each imageFile As String In imgFiles

                Dim fileName As String = sanitize(imageFile.Trim.Replace(path, ""))

                If fileName.Contains("Thumbs.db") Then

                Else

                    Dim numericCheck As Integer

                    If fileName.Contains("-") Then

                        Dim array = fileName.Split("-")

                        If Integer.TryParse(array(1).Trim, numericCheck) Then

                        Else

                            failedImages.Add(fileName.Trim)
                        End If
                    Else

                        failedImages.Add(fileName.Trim)
                    End If
                End If
            Next
        End If
    End Sub

    Public Sub RefreshCheck(sender As Object, e As KeyEventArgs)

        If e.KeyCode = Keys.F5 Then

            CheckNewImages()
            e.SuppressKeyPress = True
        End If
    End Sub

    Private Sub Helper()

        MsgBox("Upload New Images:" + vbCr + "Uploads all new images to database, moves them from 'Pending' to 'Completed Presentation' folder." + vbCr +
               "   - Press 'F5' to check for new images (Enables Upload Button)." + vbCr + vbCr +
               "Picture Checker:" + vbCr + "Gives an overview of all images in 'Out Of Stock' folder that should be in 'Works', and vice versa." + vbCr +
               "   - Allows you to move images to the correct folder." + vbCr +
               "   - Press 'F5' to refresh grid for latest images.", MsgBoxStyle.Information, "Information")
    End Sub

    Private Sub OpenPictureChecker()

        Dim pic As New PictureChecker
        AddHandler Me.KeyDown, AddressOf pic.refreshKeyDown
        RemoveHandler Me.KeyDown, AddressOf RefreshCheck
        Me.Controls.Add(pic)
        pic.BringToFront()
        pic.Dock = DockStyle.Fill
    End Sub

    Private Sub UploadImages()

        If bgw_uploadFiles.IsBusy Or bgw_finishUploading.IsBusy Or bgw_cloudinaryLoader.IsBusy Or bgw_blueAlligator.IsBusy Or bgw_blueAlligatorCustomers.IsBusy Then

            MsgBox("Upload In Progress - Please Wait", MsgBoxStyle.Information, "Uploading")
        Else

            Dim imgFiles() As String = Directory.GetFiles(path)

            productList.Clear()
            fileList.Clear()

            For Each img As String In imgFiles

                Dim skip As Boolean = False

                For Each i As String In failedImages

                    If img.Contains(i.Trim) Then

                        skip = True
                    End If
                Next

                If skip = False Then

                    Dim trimmed As String

                    img = img.Replace(path, "")

                    If Not (img.Contains(".db")) Then

                        fileList.Add(img.Trim)
                        trimmed = sanitize(img)

                        If Not (productList.Contains(trimmed.Trim)) Then

                            productList.Add(trimmed.Trim)
                        End If
                    End If
                End If
            Next

            img_uploader.Visible = True
            bgw_uploadFiles.RunWorkerAsync()
        End If
    End Sub

#Region " ------------- BACKGROUND WORKERS --------------"

    Private Sub bgw_uploadFiles_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgw_uploadFiles.DoWork

        Try
            For Each s As String In productList

                Dim temp = s.Split("-")
                Dim product = temp(0).ToString.Trim

                executeNonQueryFromPTLSAGECustomQuery("DELETE FROM PinnPriceCheck.dbo.ImageTable WHERE product = '" + product + "'", db_conx(My.Settings.connectionString))
            Next
        Catch ex As Exception

            errorMsg = ex.ToString
        End Try
    End Sub

    Private Sub bgw_uploadFiles_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgw_uploadFiles.RunWorkerCompleted

        If errorMsg.Trim = "" Then

            bgw_finishUploading.RunWorkerAsync()
        Else

            MsgBox("Error Occurred Uploading Images - Contact IT" + vbCr + errorMsg.Trim, MsgBoxStyle.Critical, "ERROR")
        End If
    End Sub

    Private Sub bgw_finishUploading_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgw_finishUploading.DoWork

        Try

            For Each s As String In fileList

                Dim temp = s.Split("-")
                Dim product = temp(0).ToString.Trim
                Dim name = temp(0).ToString.Trim + "-" + sanitize(temp(1).ToString.Trim)

                Dim connection As SqlConnection
                connection = New SqlConnection(db_conx(My.Settings.connectionString))

                Dim MyImage As Image = Image.FromFile(path + s)
                Dim ms As New MemoryStream()
                MyImage.Save(ms, MyImage.RawFormat)

                Dim sql As String = "INSERT INTO [PinnPriceCheck].[dbo].[ImageTable] VALUES(@product, @image_name, @image, getdate())"
                Dim cmd As New SqlCommand(sql, connection)

                cmd.Parameters.AddWithValue("@product", product)
                cmd.Parameters.AddWithValue("@image_name", name)
                cmd.Parameters.Add("@image", SqlDbType.Image).Value = ms.ToArray()

                cmd.Connection = connection
                connection.Open()
                cmd.ExecuteNonQuery()
                connection.Close()

                MyImage.Dispose()
                ms.Close()
            Next

        Catch ex As Exception

            errorMsg = ex.ToString
        End Try
    End Sub

    Private Sub bgw_finishUploading_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgw_finishUploading.RunWorkerCompleted

        If errorMsg.Trim = "" Then

            For Each s As String In fileList

                If File.Exists(destination + s.Trim) Then

                    File.Delete(destination + s.Trim)
                End If

                File.Move(path + s.Trim, destination + s.Trim)
            Next

            CloudinaryUploader()
        Else

            MsgBox("Error Occurred Finalising Upload - Contact IT" + vbCr + errorMsg.Trim, MsgBoxStyle.Critical, "ERROR")
        End If
    End Sub

    Private Sub bgw_cloudinaryLoader_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgw_cloudinaryLoader.DoWork

        Try

            Module1.Main()
        Catch ex As Exception

            errorMsg = ex.ToString.Trim
        End Try
    End Sub

    Private Sub bgw_cloudinaryLoader_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgw_cloudinaryLoader.RunWorkerCompleted

        If errorMsg.Trim = "" Then

            BlueAlligatorUploader()
        Else

            MsgBox(errorMsg.Trim, MsgBoxStyle.Exclamation, "ERROR")
        End If
    End Sub

    Private Sub bgw_blueAlligator_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgw_blueAlligator.DoWork

        Try

            Dim p = Process.Start("\\PTLSTORE\Public\programs\AutoLoadImages.exe")

        Catch ex As Exception

            errorMsg = ex.ToString
        End Try
    End Sub

    Private Sub bgw_blueAlligator_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgw_blueAlligator.RunWorkerCompleted

        If errorMsg = "" Then

            BlueAlligatorCustomerUploader()
        Else

            MsgBox(errorMsg.Trim, MsgBoxStyle.Exclamation, "ERROR")
        End If
    End Sub

    Private Sub bgw_blueAlligatorCustomers_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgw_blueAlligatorCustomers.DoWork

        Try
            Dim open As Boolean = True

            Do While open = True

                Dim p() As Process

                p = Process.GetProcessesByName("AutoLoadImages")

                If p.Count = 0 Then

                    open = False
                End If
            Loop

            Process.Start("\\PTLSTORE\Public\programs\AutoUploaderCustomer.exe")

        Catch ex As Exception

            errorMsg = ex.ToString
        End Try
    End Sub

    Private Sub bgw_blueAlligatorCustomers_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgw_blueAlligatorCustomers.RunWorkerCompleted

        If errorMsg = "" Then

            Dim open As Boolean = True

            Do While open = True

                Dim p() As Process

                p = Process.GetProcessesByName("AutoUploaderCustomer")

                If p.Count = 0 Then

                    open = False
                End If
            Loop

            img_uploader.Visible = False

            If failedImages.Count > 0 Then

                Dim imgList As String = ""

                For i = 0 To failedImages.Count - 1

                    If i <> failedImages.Count - 1 Then

                        imgList += failedImages(i).ToString.Trim + ", "
                    Else

                        imgList += failedImages(i).ToString.Trim
                    End If
                Next

                imgList = imgList.Replace("Thumbs.db", "")

                If imgList.Trim <> "" And imgList.Trim <> "," Then

                    MsgBox("Couldn't Upload The Following Images: " + vbCr + imgList.Trim + vbCr + "Please Make Sure The Naming Conventions Are Correct In All Folders", MsgBoxStyle.Information, "Failed Upload")
                End If
            End If

            CheckNewImages()
        Else

            MsgBox(errorMsg.Trim, MsgBoxStyle.Exclamation, "ERROR")
        End If
    End Sub


#End Region
End Class
