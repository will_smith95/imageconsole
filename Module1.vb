﻿Imports System.IO
Imports System.Net
Imports System.Security.Cryptography.X509Certificates
Imports CloudinaryDotNet
Imports CloudinaryDotNet.Actions

Module Module1

    Sub Main()
        Dim account As Account = New Account(
  "pricecheck",
  "886421511979563",
  "cggD6ZUVVADvU-NtJjJGZMNebfY")

        Dim cloudinary As Cloudinary = New Cloudinary(account)

        Dim filePath As String = "\\PTLSTORE\Public\Photos & images\PHOTOBOX\CLOUDINARY PENDING\"
        Dim completed As String = "\\PTLSTORE\Public\Photos & images\PHOTOBOX\CLOUDINARY COMPLETED\"
        Dim di As New DirectoryInfo(filePath)
        Dim fiArr As FileInfo() = di.GetFiles()
        Dim i As Integer = 0
        Dim failedImages As New ArrayList
        Dim fri As FileInfo

        Dim imgFiles() As String = Directory.GetFiles(filePath)
        failedImages.Clear()

        For Each imageFile As String In imgFiles

            Dim fileName As String = sanitize(imageFile.Trim.Replace(filePath, ""))

            If fileName.Contains("Thumbs.db") Then

            Else

                Dim numericCheck As Integer

                If fileName.Contains("-") Then

                    Dim array = fileName.Split("-")

                    If Integer.TryParse(array(1).Trim, numericCheck) Then

                    Else

                        failedImages.Add(fileName.Trim)
                    End If
                Else

                    failedImages.Add(fileName.Trim)
                End If
            End If
        Next

        For Each fri In fiArr

            If fri.Name <> "Thumbs.db" Then

                Dim skip As Boolean = False

                For Each im As String In failedImages

                    If fri.Name.Contains(im.Trim) Then

                        skip = True
                    End If
                Next

                If skip = False Then

                    Dim uploadParams = New ImageUploadParams
                    uploadParams.File = New FileDescription(fri.FullName)

                    uploadParams.PublicId = sanitize(fri.Name)
                    uploadParams.Transformation =
                      New Transformation().Crop("limit").Width(2048).Height(2048)
                    uploadParams.EagerTransforms = New List(Of Transformation)
                    uploadParams.Overwrite = True
                    uploadParams.Invalidate = True

                    i = i + 1

                    Dim uploadResult = cloudinary.Upload(uploadParams)

                    If System.IO.File.Exists(fri.FullName) = True Then

                        If System.IO.File.Exists(completed + fri.Name) Then

                            System.IO.File.Delete(completed + fri.Name)
                        End If

                        System.IO.File.Move(filePath + fri.Name, completed + fri.Name)
                    End If
                End If
            End If
        Next fri
    End Sub
End Module
