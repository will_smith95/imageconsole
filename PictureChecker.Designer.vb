﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PictureChecker
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PictureChecker))
        Me.RadPageView1 = New Telerik.WinControls.UI.RadPageView()
        Me.RadPageViewPage1 = New Telerik.WinControls.UI.RadPageViewPage()
        Me.RadSplitContainer1 = New Telerik.WinControls.UI.RadSplitContainer()
        Me.SplitPanel1 = New Telerik.WinControls.UI.SplitPanel()
        Me.lbl_additions = New Telerik.WinControls.UI.RadLabel()
        Me.btn_refreshAdditions = New Telerik.WinControls.UI.RadButton()
        Me.btn_picCheck = New Telerik.WinControls.UI.RadButton()
        Me.SplitPanel2 = New Telerik.WinControls.UI.SplitPanel()
        Me.rgv_additions = New Telerik.WinControls.UI.RadGridView()
        Me.img_additions = New System.Windows.Forms.PictureBox()
        Me.RadButton2 = New Telerik.WinControls.UI.RadButton()
        Me.RadPageViewPage2 = New Telerik.WinControls.UI.RadPageViewPage()
        Me.RadSplitContainer2 = New Telerik.WinControls.UI.RadSplitContainer()
        Me.SplitPanel3 = New Telerik.WinControls.UI.SplitPanel()
        Me.btn_refreshRemovals = New Telerik.WinControls.UI.RadButton()
        Me.lbl_removals = New Telerik.WinControls.UI.RadLabel()
        Me.btn_remCheck = New Telerik.WinControls.UI.RadButton()
        Me.SplitPanel4 = New Telerik.WinControls.UI.SplitPanel()
        Me.rgv_removals = New Telerik.WinControls.UI.RadGridView()
        Me.img_removals = New System.Windows.Forms.PictureBox()
        Me.RadPageViewPage3 = New Telerik.WinControls.UI.RadPageViewPage()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.chk_bothDB = New Telerik.WinControls.UI.RadCheckBox()
        Me.lbl_database = New Telerik.WinControls.UI.RadLabel()
        Me.btn_refreshDB = New Telerik.WinControls.UI.RadButton()
        Me.rgv_database = New Telerik.WinControls.UI.RadGridView()
        Me.img_dbLoad = New System.Windows.Forms.PictureBox()
        Me.RadPageViewPage4 = New Telerik.WinControls.UI.RadPageViewPage()
        Me.RadSplitContainer3 = New Telerik.WinControls.UI.RadSplitContainer()
        Me.SplitPanel5 = New Telerik.WinControls.UI.SplitPanel()
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.SplitPanel6 = New Telerik.WinControls.UI.SplitPanel()
        Me.rgv_cloudinary = New Telerik.WinControls.UI.RadGridView()
        Me.img_cloudinary = New System.Windows.Forms.PictureBox()
        Me.bgw_additions = New System.ComponentModel.BackgroundWorker()
        Me.bgw_removals = New System.ComponentModel.BackgroundWorker()
        Me.bgw_databaseLoader = New System.ComponentModel.BackgroundWorker()
        Me.bgw_moveToWorks = New System.ComponentModel.BackgroundWorker()
        Me.bgw_moveToStock = New System.ComponentModel.BackgroundWorker()
        Me.bgw_cloudinaryLoader = New System.ComponentModel.BackgroundWorker()
        CType(Me.RadPageView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPageViewPage1.SuspendLayout()
        CType(Me.RadSplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbl_additions, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_refreshAdditions, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_picCheck, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgv_additions, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgv_additions.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_additions, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPageViewPage2.SuspendLayout()
        CType(Me.RadSplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitPanel3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_refreshRemovals, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbl_removals, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_remCheck, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitPanel4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgv_removals, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgv_removals.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_removals, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPageViewPage3.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.chk_bothDB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbl_database, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_refreshDB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgv_database, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgv_database.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_dbLoad, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPageViewPage4.SuspendLayout()
        CType(Me.RadSplitContainer3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitPanel5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitPanel6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgv_cloudinary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgv_cloudinary.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_cloudinary, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RadPageView1
        '
        Me.RadPageView1.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.RadPageView1.Controls.Add(Me.RadPageViewPage1)
        Me.RadPageView1.Controls.Add(Me.RadPageViewPage2)
        Me.RadPageView1.Controls.Add(Me.RadPageViewPage3)
        Me.RadPageView1.Controls.Add(Me.RadPageViewPage4)
        Me.RadPageView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RadPageView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadPageView1.Location = New System.Drawing.Point(0, 0)
        Me.RadPageView1.Name = "RadPageView1"
        '
        '
        '
        Me.RadPageView1.RootElement.AccessibleDescription = Nothing
        Me.RadPageView1.RootElement.AccessibleName = Nothing
        Me.RadPageView1.RootElement.ControlBounds = New System.Drawing.Rectangle(0, 0, 400, 300)
        Me.RadPageView1.SelectedPage = Me.RadPageViewPage1
        Me.RadPageView1.Size = New System.Drawing.Size(944, 625)
        Me.RadPageView1.TabIndex = 0
        Me.RadPageView1.Text = "RadPageView1"
        '
        'RadPageViewPage1
        '
        Me.RadPageViewPage1.Controls.Add(Me.RadSplitContainer1)
        Me.RadPageViewPage1.Controls.Add(Me.RadButton2)
        Me.RadPageViewPage1.Location = New System.Drawing.Point(10, 35)
        Me.RadPageViewPage1.Name = "RadPageViewPage1"
        Me.RadPageViewPage1.Size = New System.Drawing.Size(923, 579)
        Me.RadPageViewPage1.Text = "Additions"
        '
        'RadSplitContainer1
        '
        Me.RadSplitContainer1.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.RadSplitContainer1.Controls.Add(Me.SplitPanel1)
        Me.RadSplitContainer1.Controls.Add(Me.SplitPanel2)
        Me.RadSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RadSplitContainer1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadSplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.RadSplitContainer1.Name = "RadSplitContainer1"
        Me.RadSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        '
        '
        Me.RadSplitContainer1.RootElement.AccessibleDescription = Nothing
        Me.RadSplitContainer1.RootElement.AccessibleName = Nothing
        Me.RadSplitContainer1.RootElement.ControlBounds = New System.Drawing.Rectangle(0, 0, 200, 200)
        Me.RadSplitContainer1.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.RadSplitContainer1.Size = New System.Drawing.Size(923, 579)
        Me.RadSplitContainer1.SplitterWidth = 4
        Me.RadSplitContainer1.TabIndex = 4
        Me.RadSplitContainer1.TabStop = False
        Me.RadSplitContainer1.Text = "RadSplitContainer1"
        '
        'SplitPanel1
        '
        Me.SplitPanel1.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.SplitPanel1.Controls.Add(Me.lbl_additions)
        Me.SplitPanel1.Controls.Add(Me.btn_refreshAdditions)
        Me.SplitPanel1.Controls.Add(Me.btn_picCheck)
        Me.SplitPanel1.Location = New System.Drawing.Point(0, 0)
        Me.SplitPanel1.Name = "SplitPanel1"
        '
        '
        '
        Me.SplitPanel1.RootElement.AccessibleDescription = Nothing
        Me.SplitPanel1.RootElement.AccessibleName = Nothing
        Me.SplitPanel1.RootElement.ControlBounds = New System.Drawing.Rectangle(0, 0, 200, 200)
        Me.SplitPanel1.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.SplitPanel1.Size = New System.Drawing.Size(923, 75)
        Me.SplitPanel1.SizeInfo.AutoSizeScale = New System.Drawing.SizeF(0.0!, -0.36911!)
        Me.SplitPanel1.SizeInfo.SplitterCorrection = New System.Drawing.Size(0, -211)
        Me.SplitPanel1.TabIndex = 0
        Me.SplitPanel1.TabStop = False
        Me.SplitPanel1.Text = "SplitPanel1"
        '
        'lbl_additions
        '
        Me.lbl_additions.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lbl_additions.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_additions.Location = New System.Drawing.Point(187, 16)
        Me.lbl_additions.Name = "lbl_additions"
        '
        '
        '
        Me.lbl_additions.RootElement.AccessibleDescription = Nothing
        Me.lbl_additions.RootElement.AccessibleName = Nothing
        Me.lbl_additions.RootElement.ControlBounds = New System.Drawing.Rectangle(187, 16, 100, 18)
        Me.lbl_additions.Size = New System.Drawing.Size(597, 46)
        Me.lbl_additions.TabIndex = 8
        Me.lbl_additions.Text = resources.GetString("lbl_additions.Text")
        Me.lbl_additions.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_refreshAdditions
        '
        Me.btn_refreshAdditions.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_refreshAdditions.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_refreshAdditions.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.btn_refreshAdditions.Location = New System.Drawing.Point(4408, 20)
        Me.btn_refreshAdditions.Name = "btn_refreshAdditions"
        '
        '
        '
        Me.btn_refreshAdditions.RootElement.AccessibleDescription = Nothing
        Me.btn_refreshAdditions.RootElement.AccessibleName = Nothing
        Me.btn_refreshAdditions.RootElement.ControlBounds = New System.Drawing.Rectangle(3685, 20, 110, 24)
        Me.btn_refreshAdditions.Size = New System.Drawing.Size(68, 37)
        Me.btn_refreshAdditions.TabIndex = 7
        Me.btn_refreshAdditions.Text = "Refresh"
        '
        'btn_picCheck
        '
        Me.btn_picCheck.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_picCheck.Enabled = False
        Me.btn_picCheck.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.btn_picCheck.Location = New System.Drawing.Point(13, 20)
        Me.btn_picCheck.Name = "btn_picCheck"
        '
        '
        '
        Me.btn_picCheck.RootElement.AccessibleDescription = Nothing
        Me.btn_picCheck.RootElement.AccessibleName = Nothing
        Me.btn_picCheck.RootElement.ControlBounds = New System.Drawing.Rectangle(13, 20, 110, 24)
        Me.btn_picCheck.RootElement.Enabled = False
        Me.btn_picCheck.Size = New System.Drawing.Size(166, 37)
        Me.btn_picCheck.TabIndex = 2
        Me.btn_picCheck.Text = "Move From Out Of Stock"
        '
        'SplitPanel2
        '
        Me.SplitPanel2.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.SplitPanel2.Controls.Add(Me.rgv_additions)
        Me.SplitPanel2.Location = New System.Drawing.Point(0, 79)
        Me.SplitPanel2.Name = "SplitPanel2"
        '
        '
        '
        Me.SplitPanel2.RootElement.AccessibleDescription = Nothing
        Me.SplitPanel2.RootElement.AccessibleName = Nothing
        Me.SplitPanel2.RootElement.ControlBounds = New System.Drawing.Rectangle(0, 79, 200, 200)
        Me.SplitPanel2.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.SplitPanel2.Size = New System.Drawing.Size(923, 500)
        Me.SplitPanel2.SizeInfo.AutoSizeScale = New System.Drawing.SizeF(0.0!, 0.36911!)
        Me.SplitPanel2.SizeInfo.SplitterCorrection = New System.Drawing.Size(0, 211)
        Me.SplitPanel2.TabIndex = 1
        Me.SplitPanel2.TabStop = False
        Me.SplitPanel2.Text = "SplitPanel2"
        '
        'rgv_additions
        '
        Me.rgv_additions.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.rgv_additions.Controls.Add(Me.img_additions)
        Me.rgv_additions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rgv_additions.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rgv_additions.Location = New System.Drawing.Point(0, 0)
        '
        'rgv_additions
        '
        Me.rgv_additions.MasterTemplate.AllowAddNewRow = False
        Me.rgv_additions.MasterTemplate.AllowCellContextMenu = False
        Me.rgv_additions.MasterTemplate.AllowColumnChooser = False
        Me.rgv_additions.MasterTemplate.AllowColumnHeaderContextMenu = False
        Me.rgv_additions.MasterTemplate.AllowColumnReorder = False
        Me.rgv_additions.MasterTemplate.AllowColumnResize = False
        Me.rgv_additions.MasterTemplate.AllowDeleteRow = False
        Me.rgv_additions.MasterTemplate.AllowDragToGroup = False
        Me.rgv_additions.MasterTemplate.AllowEditRow = False
        Me.rgv_additions.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill
        Me.rgv_additions.Name = "rgv_additions"
        '
        '
        '
        Me.rgv_additions.RootElement.AccessibleDescription = Nothing
        Me.rgv_additions.RootElement.AccessibleName = Nothing
        Me.rgv_additions.RootElement.ControlBounds = New System.Drawing.Rectangle(0, 0, 240, 150)
        Me.rgv_additions.Size = New System.Drawing.Size(923, 500)
        Me.rgv_additions.TabIndex = 0
        Me.rgv_additions.Text = "RadGridView1"
        '
        'img_additions
        '
        Me.img_additions.Image = CType(resources.GetObject("img_additions.Image"), System.Drawing.Image)
        Me.img_additions.Location = New System.Drawing.Point(386, 176)
        Me.img_additions.Name = "img_additions"
        Me.img_additions.Size = New System.Drawing.Size(177, 149)
        Me.img_additions.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.img_additions.TabIndex = 1
        Me.img_additions.TabStop = False
        Me.img_additions.Visible = False
        '
        'RadButton2
        '
        Me.RadButton2.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.RadButton2.Enabled = False
        Me.RadButton2.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.RadButton2.Location = New System.Drawing.Point(13, 20)
        Me.RadButton2.Name = "RadButton2"
        '
        '
        '
        Me.RadButton2.RootElement.AccessibleDescription = Nothing
        Me.RadButton2.RootElement.AccessibleName = Nothing
        Me.RadButton2.RootElement.ControlBounds = New System.Drawing.Rectangle(13, 20, 110, 24)
        Me.RadButton2.RootElement.Enabled = False
        Me.RadButton2.Size = New System.Drawing.Size(166, 37)
        Me.RadButton2.TabIndex = 6
        Me.RadButton2.Text = "Move From Out Of Stock"
        '
        'RadPageViewPage2
        '
        Me.RadPageViewPage2.Controls.Add(Me.RadSplitContainer2)
        Me.RadPageViewPage2.Location = New System.Drawing.Point(10, 37)
        Me.RadPageViewPage2.Name = "RadPageViewPage2"
        Me.RadPageViewPage2.Size = New System.Drawing.Size(900, 577)
        Me.RadPageViewPage2.Text = "Removals"
        '
        'RadSplitContainer2
        '
        Me.RadSplitContainer2.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.RadSplitContainer2.Controls.Add(Me.SplitPanel3)
        Me.RadSplitContainer2.Controls.Add(Me.SplitPanel4)
        Me.RadSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RadSplitContainer2.Location = New System.Drawing.Point(0, 0)
        Me.RadSplitContainer2.Name = "RadSplitContainer2"
        Me.RadSplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        '
        '
        Me.RadSplitContainer2.RootElement.AccessibleDescription = Nothing
        Me.RadSplitContainer2.RootElement.AccessibleName = Nothing
        Me.RadSplitContainer2.RootElement.ControlBounds = New System.Drawing.Rectangle(0, 0, 200, 200)
        Me.RadSplitContainer2.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.RadSplitContainer2.Size = New System.Drawing.Size(900, 577)
        Me.RadSplitContainer2.SplitterWidth = 4
        Me.RadSplitContainer2.TabIndex = 0
        Me.RadSplitContainer2.TabStop = False
        Me.RadSplitContainer2.Text = "RadSplitContainer2"
        '
        'SplitPanel3
        '
        Me.SplitPanel3.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.SplitPanel3.Controls.Add(Me.btn_refreshRemovals)
        Me.SplitPanel3.Controls.Add(Me.lbl_removals)
        Me.SplitPanel3.Controls.Add(Me.btn_remCheck)
        Me.SplitPanel3.Location = New System.Drawing.Point(0, 0)
        Me.SplitPanel3.Name = "SplitPanel3"
        '
        '
        '
        Me.SplitPanel3.RootElement.AccessibleDescription = Nothing
        Me.SplitPanel3.RootElement.AccessibleName = Nothing
        Me.SplitPanel3.RootElement.ControlBounds = New System.Drawing.Rectangle(0, 0, 200, 200)
        Me.SplitPanel3.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.SplitPanel3.Size = New System.Drawing.Size(900, 75)
        Me.SplitPanel3.SizeInfo.AutoSizeScale = New System.Drawing.SizeF(0.0!, -0.36911!)
        Me.SplitPanel3.SizeInfo.SplitterCorrection = New System.Drawing.Size(0, -211)
        Me.SplitPanel3.TabIndex = 0
        Me.SplitPanel3.TabStop = False
        Me.SplitPanel3.Text = "SplitPanel3"
        '
        'btn_refreshRemovals
        '
        Me.btn_refreshRemovals.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_refreshRemovals.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_refreshRemovals.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.btn_refreshRemovals.Location = New System.Drawing.Point(4316, 20)
        Me.btn_refreshRemovals.Name = "btn_refreshRemovals"
        '
        '
        '
        Me.btn_refreshRemovals.RootElement.AccessibleDescription = Nothing
        Me.btn_refreshRemovals.RootElement.AccessibleName = Nothing
        Me.btn_refreshRemovals.RootElement.ControlBounds = New System.Drawing.Rectangle(3616, 20, 110, 24)
        Me.btn_refreshRemovals.Size = New System.Drawing.Size(68, 37)
        Me.btn_refreshRemovals.TabIndex = 5
        Me.btn_refreshRemovals.Text = "Refresh"
        '
        'lbl_removals
        '
        Me.lbl_removals.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lbl_removals.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.lbl_removals.Location = New System.Drawing.Point(148, 20)
        Me.lbl_removals.Name = "lbl_removals"
        '
        '
        '
        Me.lbl_removals.RootElement.AccessibleDescription = Nothing
        Me.lbl_removals.RootElement.AccessibleName = Nothing
        Me.lbl_removals.RootElement.ControlBounds = New System.Drawing.Rectangle(148, 20, 100, 18)
        Me.lbl_removals.Size = New System.Drawing.Size(480, 38)
        Me.lbl_removals.TabIndex = 9
        Me.lbl_removals.Text = "<html>These products are not available   but are in the 'Works' folder.<br />The " & _
    "'Move Works' button will move any images to 'Out Of Stock' From 'Works'.</html>"
        Me.lbl_removals.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_remCheck
        '
        Me.btn_remCheck.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_remCheck.Enabled = False
        Me.btn_remCheck.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.btn_remCheck.Location = New System.Drawing.Point(13, 20)
        Me.btn_remCheck.Name = "btn_remCheck"
        '
        '
        '
        Me.btn_remCheck.RootElement.AccessibleDescription = Nothing
        Me.btn_remCheck.RootElement.AccessibleName = Nothing
        Me.btn_remCheck.RootElement.ControlBounds = New System.Drawing.Rectangle(13, 20, 110, 24)
        Me.btn_remCheck.RootElement.Enabled = False
        Me.btn_remCheck.Size = New System.Drawing.Size(129, 37)
        Me.btn_remCheck.TabIndex = 4
        Me.btn_remCheck.Text = "Move From Works"
        '
        'SplitPanel4
        '
        Me.SplitPanel4.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.SplitPanel4.Controls.Add(Me.rgv_removals)
        Me.SplitPanel4.Location = New System.Drawing.Point(0, 79)
        Me.SplitPanel4.Name = "SplitPanel4"
        '
        '
        '
        Me.SplitPanel4.RootElement.AccessibleDescription = Nothing
        Me.SplitPanel4.RootElement.AccessibleName = Nothing
        Me.SplitPanel4.RootElement.ControlBounds = New System.Drawing.Rectangle(0, 79, 200, 200)
        Me.SplitPanel4.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.SplitPanel4.Size = New System.Drawing.Size(900, 498)
        Me.SplitPanel4.SizeInfo.AutoSizeScale = New System.Drawing.SizeF(0.0!, 0.36911!)
        Me.SplitPanel4.SizeInfo.SplitterCorrection = New System.Drawing.Size(0, 211)
        Me.SplitPanel4.TabIndex = 1
        Me.SplitPanel4.TabStop = False
        Me.SplitPanel4.Text = "SplitPanel4"
        '
        'rgv_removals
        '
        Me.rgv_removals.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.rgv_removals.Controls.Add(Me.img_removals)
        Me.rgv_removals.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rgv_removals.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rgv_removals.Location = New System.Drawing.Point(0, 0)
        '
        '
        '
        Me.rgv_removals.MasterTemplate.AllowAddNewRow = False
        Me.rgv_removals.MasterTemplate.AllowCellContextMenu = False
        Me.rgv_removals.MasterTemplate.AllowColumnChooser = False
        Me.rgv_removals.MasterTemplate.AllowColumnHeaderContextMenu = False
        Me.rgv_removals.MasterTemplate.AllowColumnReorder = False
        Me.rgv_removals.MasterTemplate.AllowColumnResize = False
        Me.rgv_removals.MasterTemplate.AllowDeleteRow = False
        Me.rgv_removals.MasterTemplate.AllowDragToGroup = False
        Me.rgv_removals.MasterTemplate.AllowEditRow = False
        Me.rgv_removals.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill
        Me.rgv_removals.Name = "rgv_removals"
        '
        '
        '
        Me.rgv_removals.RootElement.AccessibleDescription = Nothing
        Me.rgv_removals.RootElement.AccessibleName = Nothing
        Me.rgv_removals.RootElement.ControlBounds = New System.Drawing.Rectangle(0, 0, 240, 150)
        Me.rgv_removals.Size = New System.Drawing.Size(900, 498)
        Me.rgv_removals.TabIndex = 2
        Me.rgv_removals.Text = "RadGridView1"
        '
        'img_removals
        '
        Me.img_removals.Image = CType(resources.GetObject("img_removals.Image"), System.Drawing.Image)
        Me.img_removals.Location = New System.Drawing.Point(386, 176)
        Me.img_removals.Name = "img_removals"
        Me.img_removals.Size = New System.Drawing.Size(177, 149)
        Me.img_removals.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.img_removals.TabIndex = 2
        Me.img_removals.TabStop = False
        Me.img_removals.Visible = False
        '
        'RadPageViewPage3
        '
        Me.RadPageViewPage3.Controls.Add(Me.SplitContainer1)
        Me.RadPageViewPage3.Location = New System.Drawing.Point(10, 37)
        Me.RadPageViewPage3.Name = "RadPageViewPage3"
        Me.RadPageViewPage3.Size = New System.Drawing.Size(900, 577)
        Me.RadPageViewPage3.Text = "Missing From Database"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.chk_bothDB)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lbl_database)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btn_refreshDB)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.rgv_database)
        Me.SplitContainer1.Size = New System.Drawing.Size(900, 577)
        Me.SplitContainer1.SplitterDistance = 75
        Me.SplitContainer1.TabIndex = 0
        '
        'chk_bothDB
        '
        Me.chk_bothDB.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chk_bothDB.AutoSize = False
        Me.chk_bothDB.BackColor = System.Drawing.Color.Transparent
        Me.chk_bothDB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.chk_bothDB.Location = New System.Drawing.Point(679, 39)
        Me.chk_bothDB.Name = "chk_bothDB"
        '
        '
        '
        Me.chk_bothDB.RootElement.AccessibleDescription = Nothing
        Me.chk_bothDB.RootElement.AccessibleName = Nothing
        Me.chk_bothDB.RootElement.ControlBounds = New System.Drawing.Rectangle(679, 39, 100, 18)
        Me.chk_bothDB.Size = New System.Drawing.Size(130, 16)
        Me.chk_bothDB.TabIndex = 11
        Me.chk_bothDB.Text = "Check 2nd Database"
        '
        'lbl_database
        '
        Me.lbl_database.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lbl_database.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.lbl_database.Location = New System.Drawing.Point(13, 19)
        Me.lbl_database.Name = "lbl_database"
        '
        '
        '
        Me.lbl_database.RootElement.AccessibleDescription = Nothing
        Me.lbl_database.RootElement.AccessibleName = Nothing
        Me.lbl_database.RootElement.ControlBounds = New System.Drawing.Rectangle(13, 19, 100, 18)
        Me.lbl_database.Size = New System.Drawing.Size(450, 38)
        Me.lbl_database.TabIndex = 10
        Me.lbl_database.Text = "<html>These products are available but are not in the Image Database.<br />These " & _
    "products will need photos taking and uploading to the Database.</html>"
        Me.lbl_database.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_refreshDB
        '
        Me.btn_refreshDB.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_refreshDB.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_refreshDB.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.btn_refreshDB.Location = New System.Drawing.Point(816, 20)
        Me.btn_refreshDB.Name = "btn_refreshDB"
        '
        '
        '
        Me.btn_refreshDB.RootElement.AccessibleDescription = Nothing
        Me.btn_refreshDB.RootElement.AccessibleName = Nothing
        Me.btn_refreshDB.RootElement.ControlBounds = New System.Drawing.Rectangle(816, 20, 110, 24)
        Me.btn_refreshDB.Size = New System.Drawing.Size(68, 37)
        Me.btn_refreshDB.TabIndex = 7
        Me.btn_refreshDB.Text = "Refresh"
        '
        'rgv_database
        '
        Me.rgv_database.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.rgv_database.Controls.Add(Me.img_dbLoad)
        Me.rgv_database.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rgv_database.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rgv_database.Location = New System.Drawing.Point(0, 0)
        '
        '
        '
        Me.rgv_database.MasterTemplate.AllowAddNewRow = False
        Me.rgv_database.MasterTemplate.AllowCellContextMenu = False
        Me.rgv_database.MasterTemplate.AllowColumnChooser = False
        Me.rgv_database.MasterTemplate.AllowColumnHeaderContextMenu = False
        Me.rgv_database.MasterTemplate.AllowColumnReorder = False
        Me.rgv_database.MasterTemplate.AllowColumnResize = False
        Me.rgv_database.MasterTemplate.AllowDeleteRow = False
        Me.rgv_database.MasterTemplate.AllowDragToGroup = False
        Me.rgv_database.MasterTemplate.AllowEditRow = False
        Me.rgv_database.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill
        Me.rgv_database.Name = "rgv_database"
        '
        '
        '
        Me.rgv_database.RootElement.AccessibleDescription = Nothing
        Me.rgv_database.RootElement.AccessibleName = Nothing
        Me.rgv_database.RootElement.ControlBounds = New System.Drawing.Rectangle(0, 0, 240, 150)
        Me.rgv_database.Size = New System.Drawing.Size(900, 498)
        Me.rgv_database.TabIndex = 4
        Me.rgv_database.Text = "RadGridView1"
        '
        'img_dbLoad
        '
        Me.img_dbLoad.Image = CType(resources.GetObject("img_dbLoad.Image"), System.Drawing.Image)
        Me.img_dbLoad.Location = New System.Drawing.Point(386, 176)
        Me.img_dbLoad.Name = "img_dbLoad"
        Me.img_dbLoad.Size = New System.Drawing.Size(177, 149)
        Me.img_dbLoad.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.img_dbLoad.TabIndex = 3
        Me.img_dbLoad.TabStop = False
        Me.img_dbLoad.Visible = False
        '
        'RadPageViewPage4
        '
        Me.RadPageViewPage4.Controls.Add(Me.RadSplitContainer3)
        Me.RadPageViewPage4.Location = New System.Drawing.Point(10, 37)
        Me.RadPageViewPage4.Name = "RadPageViewPage4"
        Me.RadPageViewPage4.Size = New System.Drawing.Size(900, 577)
        Me.RadPageViewPage4.Text = "Cloudinary"
        '
        'RadSplitContainer3
        '
        Me.RadSplitContainer3.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.RadSplitContainer3.Controls.Add(Me.SplitPanel5)
        Me.RadSplitContainer3.Controls.Add(Me.SplitPanel6)
        Me.RadSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RadSplitContainer3.Location = New System.Drawing.Point(0, 0)
        Me.RadSplitContainer3.Name = "RadSplitContainer3"
        Me.RadSplitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        '
        '
        Me.RadSplitContainer3.RootElement.AccessibleDescription = Nothing
        Me.RadSplitContainer3.RootElement.AccessibleName = Nothing
        Me.RadSplitContainer3.RootElement.ControlBounds = New System.Drawing.Rectangle(0, 0, 200, 200)
        Me.RadSplitContainer3.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.RadSplitContainer3.Size = New System.Drawing.Size(900, 577)
        Me.RadSplitContainer3.SplitterWidth = 4
        Me.RadSplitContainer3.TabIndex = 0
        Me.RadSplitContainer3.TabStop = False
        Me.RadSplitContainer3.Text = "RadSplitContainer3"
        '
        'SplitPanel5
        '
        Me.SplitPanel5.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.SplitPanel5.Controls.Add(Me.RadLabel1)
        Me.SplitPanel5.Location = New System.Drawing.Point(0, 0)
        Me.SplitPanel5.Name = "SplitPanel5"
        '
        '
        '
        Me.SplitPanel5.RootElement.AccessibleDescription = Nothing
        Me.SplitPanel5.RootElement.AccessibleName = Nothing
        Me.SplitPanel5.RootElement.ControlBounds = New System.Drawing.Rectangle(0, 0, 200, 200)
        Me.SplitPanel5.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.SplitPanel5.Size = New System.Drawing.Size(900, 75)
        Me.SplitPanel5.SizeInfo.AutoSizeScale = New System.Drawing.SizeF(0.0!, -0.36911!)
        Me.SplitPanel5.SizeInfo.SplitterCorrection = New System.Drawing.Size(0, -211)
        Me.SplitPanel5.TabIndex = 0
        Me.SplitPanel5.TabStop = False
        Me.SplitPanel5.Text = "SplitPanel5"
        '
        'RadLabel1
        '
        Me.RadLabel1.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.RadLabel1.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.RadLabel1.Location = New System.Drawing.Point(13, 20)
        Me.RadLabel1.Name = "RadLabel1"
        '
        '
        '
        Me.RadLabel1.RootElement.AccessibleDescription = Nothing
        Me.RadLabel1.RootElement.AccessibleName = Nothing
        Me.RadLabel1.RootElement.ControlBounds = New System.Drawing.Rectangle(13, 20, 100, 18)
        Me.RadLabel1.Size = New System.Drawing.Size(336, 38)
        Me.RadLabel1.TabIndex = 11
        Me.RadLabel1.Text = "<html>These products are available but are not on Cloudinary.<br />These products" & _
    " will need photos taking and uploading.</html>"
        Me.RadLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SplitPanel6
        '
        Me.SplitPanel6.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.SplitPanel6.Controls.Add(Me.rgv_cloudinary)
        Me.SplitPanel6.Location = New System.Drawing.Point(0, 79)
        Me.SplitPanel6.Name = "SplitPanel6"
        '
        '
        '
        Me.SplitPanel6.RootElement.AccessibleDescription = Nothing
        Me.SplitPanel6.RootElement.AccessibleName = Nothing
        Me.SplitPanel6.RootElement.ControlBounds = New System.Drawing.Rectangle(0, 79, 200, 200)
        Me.SplitPanel6.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.SplitPanel6.Size = New System.Drawing.Size(900, 498)
        Me.SplitPanel6.SizeInfo.AutoSizeScale = New System.Drawing.SizeF(0.0!, 0.36911!)
        Me.SplitPanel6.SizeInfo.SplitterCorrection = New System.Drawing.Size(0, 211)
        Me.SplitPanel6.TabIndex = 1
        Me.SplitPanel6.TabStop = False
        Me.SplitPanel6.Text = "SplitPanel6"
        '
        'rgv_cloudinary
        '
        Me.rgv_cloudinary.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.rgv_cloudinary.Controls.Add(Me.img_cloudinary)
        Me.rgv_cloudinary.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rgv_cloudinary.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rgv_cloudinary.Location = New System.Drawing.Point(0, 0)
        '
        '
        '
        Me.rgv_cloudinary.MasterTemplate.AllowAddNewRow = False
        Me.rgv_cloudinary.MasterTemplate.AllowCellContextMenu = False
        Me.rgv_cloudinary.MasterTemplate.AllowColumnChooser = False
        Me.rgv_cloudinary.MasterTemplate.AllowColumnHeaderContextMenu = False
        Me.rgv_cloudinary.MasterTemplate.AllowColumnReorder = False
        Me.rgv_cloudinary.MasterTemplate.AllowColumnResize = False
        Me.rgv_cloudinary.MasterTemplate.AllowDeleteRow = False
        Me.rgv_cloudinary.MasterTemplate.AllowDragToGroup = False
        Me.rgv_cloudinary.MasterTemplate.AllowEditRow = False
        Me.rgv_cloudinary.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill
        Me.rgv_cloudinary.Name = "rgv_cloudinary"
        '
        '
        '
        Me.rgv_cloudinary.RootElement.AccessibleDescription = Nothing
        Me.rgv_cloudinary.RootElement.AccessibleName = Nothing
        Me.rgv_cloudinary.RootElement.ControlBounds = New System.Drawing.Rectangle(0, 0, 240, 150)
        Me.rgv_cloudinary.Size = New System.Drawing.Size(900, 498)
        Me.rgv_cloudinary.TabIndex = 6
        Me.rgv_cloudinary.Text = "RadGridView1"
        '
        'img_cloudinary
        '
        Me.img_cloudinary.Image = CType(resources.GetObject("img_cloudinary.Image"), System.Drawing.Image)
        Me.img_cloudinary.Location = New System.Drawing.Point(386, 176)
        Me.img_cloudinary.Name = "img_cloudinary"
        Me.img_cloudinary.Size = New System.Drawing.Size(177, 149)
        Me.img_cloudinary.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.img_cloudinary.TabIndex = 3
        Me.img_cloudinary.TabStop = False
        Me.img_cloudinary.Visible = False
        '
        'bgw_additions
        '
        '
        'bgw_removals
        '
        '
        'bgw_databaseLoader
        '
        '
        'bgw_moveToWorks
        '
        '
        'bgw_moveToStock
        '
        '
        'bgw_cloudinaryLoader
        '
        '
        'PictureChecker
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.RadPageView1)
        Me.Name = "PictureChecker"
        Me.Size = New System.Drawing.Size(944, 625)
        CType(Me.RadPageView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPageViewPage1.ResumeLayout(False)
        CType(Me.RadSplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbl_additions, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_refreshAdditions, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_picCheck, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgv_additions.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgv_additions, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_additions, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPageViewPage2.ResumeLayout(False)
        CType(Me.RadSplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitPanel3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_refreshRemovals, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbl_removals, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_remCheck, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitPanel4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgv_removals.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgv_removals, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_removals, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPageViewPage3.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.chk_bothDB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbl_database, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_refreshDB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgv_database.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgv_database, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_dbLoad, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPageViewPage4.ResumeLayout(False)
        CType(Me.RadSplitContainer3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitPanel5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitPanel6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgv_cloudinary.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgv_cloudinary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_cloudinary, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents bgw_additions As System.ComponentModel.BackgroundWorker
    Friend WithEvents img_additions As System.Windows.Forms.PictureBox
    Friend WithEvents bgw_removals As System.ComponentModel.BackgroundWorker
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents bgw_databaseLoader As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgw_moveToWorks As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgw_moveToStock As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgw_cloudinaryLoader As System.ComponentModel.BackgroundWorker
    Friend WithEvents img_removals As System.Windows.Forms.PictureBox
    Friend WithEvents img_dbLoad As System.Windows.Forms.PictureBox
    Friend WithEvents img_cloudinary As System.Windows.Forms.PictureBox
    Private WithEvents RadPageView1 As Telerik.WinControls.UI.RadPageView
    Private WithEvents RadPageViewPage2 As Telerik.WinControls.UI.RadPageViewPage
    Private WithEvents RadPageViewPage3 As Telerik.WinControls.UI.RadPageViewPage
    Private WithEvents RadPageViewPage4 As Telerik.WinControls.UI.RadPageViewPage
    Private WithEvents SplitPanel1 As Telerik.WinControls.UI.SplitPanel
    Private WithEvents SplitPanel2 As Telerik.WinControls.UI.SplitPanel
    Private WithEvents rgv_additions As Telerik.WinControls.UI.RadGridView
    Private WithEvents btn_picCheck As Telerik.WinControls.UI.RadButton
    Private WithEvents RadPageViewPage1 As Telerik.WinControls.UI.RadPageViewPage
    Private WithEvents RadSplitContainer1 As Telerik.WinControls.UI.RadSplitContainer
    Private WithEvents btn_refreshAdditions As Telerik.WinControls.UI.RadButton
    Private WithEvents RadButton2 As Telerik.WinControls.UI.RadButton
    Private WithEvents RadSplitContainer2 As Telerik.WinControls.UI.RadSplitContainer
    Private WithEvents SplitPanel3 As Telerik.WinControls.UI.SplitPanel
    Private WithEvents btn_refreshRemovals As Telerik.WinControls.UI.RadButton
    Private WithEvents btn_remCheck As Telerik.WinControls.UI.RadButton
    Private WithEvents SplitPanel4 As Telerik.WinControls.UI.SplitPanel
    Private WithEvents rgv_removals As Telerik.WinControls.UI.RadGridView
    Private WithEvents btn_refreshDB As Telerik.WinControls.UI.RadButton
    Private WithEvents rgv_database As Telerik.WinControls.UI.RadGridView
    Private WithEvents lbl_additions As Telerik.WinControls.UI.RadLabel
    Private WithEvents lbl_removals As Telerik.WinControls.UI.RadLabel
    Private WithEvents lbl_database As Telerik.WinControls.UI.RadLabel
    Private WithEvents RadSplitContainer3 As Telerik.WinControls.UI.RadSplitContainer
    Private WithEvents SplitPanel5 As Telerik.WinControls.UI.SplitPanel
    Private WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Private WithEvents SplitPanel6 As Telerik.WinControls.UI.SplitPanel
    Private WithEvents rgv_cloudinary As Telerik.WinControls.UI.RadGridView
    Private WithEvents chk_bothDB As Telerik.WinControls.UI.RadCheckBox
End Class
