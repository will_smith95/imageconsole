﻿Imports System.IO
Imports Telerik.WinControls.UI
Imports CloudinaryDotNet.Actions
Imports CloudinaryDotNet
Imports Newtonsoft.Json

Public Class PictureChecker

    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        initialise()
    End Sub

    Private Sub initialise()

        '\-\-\-\-\-\ HANDLER \-\-\-\-\-\
        AddHandler btn_picCheck.Click, AddressOf MoveImagesToWorks
        AddHandler btn_refreshAdditions.Click, AddressOf refreshAdditions
        AddHandler btn_remCheck.Click, AddressOf MoveImagesToStock
        AddHandler btn_refreshRemovals.Click, AddressOf refreshAdditions
        AddHandler btn_refreshDB.Click, AddressOf refreshDatabase
        AddHandler chk_bothDB.Click, AddressOf changeDatabase
        '\-\-\-\-\-\ METHODS \-\-\-\-\-\
        Additions()
        Database()
        CloudinaryImages()
    End Sub

    Dim worksPath As String = "\\PTLSTORE\Public\Photos & images\WORKS\"
    Dim stockPath As String = "\\PTLSTORE\Public\Photos & images\OUT OF STOCK\"
    Dim presPath As String = "\\PTLSTORE\Public\Photos & images\PHOTOBOX\PRESENTATION COMPLETED\"
    Dim temp As New DataTable
    Public additionsDT As New DataTable
    Public removalsDT As New DataTable
    Public databaseDT As New DataTable
    Public cloudinaryDT As New DataTable
    Dim errorMsg As String = ""
    Dim removalsErrorMsg As String = ""
    Dim databaseErrorMsg As String = ""
    Dim cloudinaryErrorMsg As String = ""

    Dim account As Account = New Account(
"pricecheck",
"886421511979563",
"cggD6ZUVVADvU-NtJjJGZMNebfY")
    Dim cloudinary As Cloudinary = New Cloudinary(Account)
    Dim both = False

    Private Sub Closer() Handles RadPageView1.PageRemoved

        RemoveHandler ParentForm.KeyDown, AddressOf refreshKeyDown
        AddHandler ParentForm.KeyDown, AddressOf Form1.RefreshCheck
        Form1.CheckNewImages()
        Me.Dispose()
    End Sub

    Private Sub changeDatabase()

        If chk_bothDB.Checked Then

            both = True
            Database()
        Else

            both = False
            refreshDatabase()
        End If
    End Sub

    Public Sub refreshKeyDown(sender As Object, e As KeyEventArgs)

        If RadPageView1.SelectedPage.Text.Trim = "Additions" Or RadPageView1.SelectedPage.Text.Trim = "Removals" Then

            If e.KeyCode = Keys.F5 Then

                refreshAdditions()
                e.SuppressKeyPress = True
            End If

        ElseIf RadPageView1.SelectedPage.Text.Trim = "Missing From Database" Then

            If e.KeyCode = Keys.F5 Then
                
                refreshDatabase()
                e.SuppressKeyPress = True
            End If
        End If
    End Sub

    Private Sub refreshAdditions()

        If bgw_additions.IsBusy Then

            MsgBox("Retrieving Images - Please Wait", MsgBoxStyle.Information, "Loading!")
        Else

            rgv_additions.DataSource = ""
            Additions()
        End If
    End Sub

    Private Sub refreshDatabase()

        If bgw_databaseLoader.IsBusy Then

            MsgBox("Retrieving Images - Please Wait", MsgBoxStyle.Information, "Loading!")
        Else

            chk_bothDB.Enabled = False
            chk_bothDB.Checked = False

            Database()
        End If
    End Sub

    Private Sub MoveImagesToWorks()

        If bgw_moveToWorks.IsBusy Then

            MsgBox("Moving Images - Please Wait", MsgBoxStyle.Information, "Moving Images!")
        Else

            img_additions.Visible = True
            bgw_moveToWorks.RunWorkerAsync()
        End If
    End Sub

    Private Sub MoveImagesToStock()

        If bgw_moveToStock.IsBusy Then

            MsgBox("Moving Images - Please Wait", MsgBoxStyle.Information, "Moving Images!")
        Else

            img_additions.Visible = True
            bgw_moveToStock.RunWorkerAsync()
        End If
    End Sub

    Private Sub Additions()
       
        If bgw_additions.IsBusy Then

            MsgBox("Retrieving Files - Please Wait", MsgBoxStyle.Information, "Loading!")
        Else

            additionsDT.Clear()
            additionsDT.Columns.Clear()
            removalsDT.Clear()
            removalsDT.Columns.Clear()
            additionsDT.Columns.Add("Product")
            additionsDT.Columns.Add("Description")
            additionsDT.Columns.Add("Physical")
            additionsDT.Columns.Add("On Order")
            additionsDT.Columns.Add("Available")
            additionsDT.Columns.Add("Bin Number")
            additionsDT.Columns.Add("Analysis A")

            errorMsg = ""
            img_additions.Visible = True
            img_removals.Visible = True
            bgw_additions.RunWorkerAsync()
        End If
    End Sub

    Private Sub Database()

        If bgw_databaseLoader.IsBusy Then

            MsgBox("Retrieving Images - Please Wait", MsgBoxStyle.Information, "Loading!")
        Else

            databaseErrorMsg = ""
            chk_bothDB.Enabled = False
            img_dbLoad.Visible = True
            rgv_database.DataSource = ""
            bgw_databaseLoader.RunWorkerAsync()
        End If
    End Sub

    Private Sub Removals()

        If bgw_removals.IsBusy Then

            MsgBox("Retrieving Files - Please Wait", MsgBoxStyle.Information, "Loading!")
        Else

            rgv_removals.DataSource = ""

            removalsDT.Clear()
            removalsDT.Columns.Clear()
            removalsDT.Columns.Add("Product")
            removalsDT.Columns.Add("Description")
            removalsDT.Columns.Add("Physical")
            removalsDT.Columns.Add("On Order")
            removalsDT.Columns.Add("Available")
            removalsDT.Columns.Add("Bin Number")
            removalsDT.Columns.Add("Analysis A")

            removalsErrorMsg = ""
            img_removals.Visible = True
            bgw_removals.RunWorkerAsync()
        End If
    End Sub

    Private Sub CloudinaryImages()

        If bgw_cloudinaryLoader.IsBusy Then

            MsgBox("Retrieving Images - Please Wait", MsgBoxStyle.Information, "Loading!")
        Else

            cloudinaryDT.Clear()
            cloudinaryDT.Columns.Clear()
            cloudinaryDT.Columns.Add("Product")
            cloudinaryDT.Columns.Add("Description")
            cloudinaryDT.Columns.Add("Physical")
            cloudinaryDT.Columns.Add("On Order")
            cloudinaryDT.Columns.Add("Available")
            cloudinaryDT.Columns.Add("Bin Number")
            cloudinaryDT.Columns.Add("Analysis A")

            cloudinaryErrorMsg = ""
            img_cloudinary.Visible = True
            bgw_cloudinaryLoader.RunWorkerAsync()
        End If
    End Sub

    Private Sub bgw_additions_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgw_additions.DoWork

        Try
            Dim ds As DataSet = getDataSetFromPTLSAGE("StockChecker", db_conx(My.Settings.connectionString))

            If ds.Tables(0).Rows.Count > 0 Then

                temp = ds.Tables(0)
                For i = 0 To ds.Tables(0).Rows.Count - 1

                    Dim productCode As String = ds.Tables(0).Rows(i)(0).ToString.Trim
                    Dim strings() As String = Directory.GetFiles(worksPath, "*" + productCode + "*")

                    If strings.Length = 0 Then

                        Dim dr As DataRow = ds.Tables(0).Rows(i)
                        additionsDT.ImportRow(dr)
                    End If
                Next
            End If
        Catch ex As Exception

            errorMsg = ex.ToString.Trim
        End Try
    End Sub

    Private Sub bgw_additions_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgw_additions.RunWorkerCompleted

        If errorMsg.Trim = "" Then

            If additionsDT.Rows.Count > 0 Then

                Removals()
                rgv_additions.DataSource = additionsDT

                For i = 0 To rgv_additions.Columns.Count - 1

                    rgv_additions.Columns(i).TextAlignment = ContentAlignment.MiddleCenter
                    rgv_additions.Columns(i).BestFit()
                    rgv_additions.Columns(i).Width += 50
                Next

                Dim found As Boolean = False

                For i = 0 To rgv_additions.Rows.Count - 1

                    Dim product As String = rgv_additions.Rows(i).Cells(0).Value.ToString.Trim
                    Dim strings() As String = Directory.GetFiles(stockPath, "*" + product + "*")

                    If strings.Length > 0 Then

                        found = True
                    End If
                Next

                If found = True Then

                    btn_picCheck.Enabled = True
                Else

                    btn_picCheck.Enabled = False
                End If
            Else

                btn_picCheck.Enabled = False
                rgv_additions.DataSource = ""
            End If
        Else

            MsgBox(errorMsg.Trim + vbCr + "Contact IT", MsgBoxStyle.Exclamation, "Error")
        End If

        img_additions.Visible = False
        img_removals.Visible = False
    End Sub

    Private Sub bgw_removals_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgw_removals.DoWork

        Try
            Dim strings() As String = Directory.GetFiles(worksPath)

            For Each s As String In strings

                If Not (s.Contains("Thumbs.db")) Then

                    Dim productCode As String = sanitize(s.Replace(worksPath, ""))
                    Dim count = 0

                    For i = 0 To temp.Rows.Count - 1

                        If temp.Rows(i)(0).ToString.Trim = productCode.Trim Then
                            count += 1
                        End If
                    Next

                    If count = 0 Then

                        Dim ds As DataSet = getDataSetFromPTLSAGE("StockChecker", db_conx(My.Settings.connectionString), "@prod", productCode.Trim)

                        If ds.Tables(0).Rows.Count > 0 Then

                            For Each dr As DataRow In ds.Tables(0).Rows

                                removalsDT.ImportRow(dr)
                            Next
                        End If
                    End If
                End If
            Next
        Catch ex As Exception

            removalsErrorMsg = ex.ToString.Trim
        End Try
    End Sub

    Private Sub bgw_removals_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgw_removals.RunWorkerCompleted

        If removalsErrorMsg.Trim = "" Then

            If removalsDT.Rows.Count > 0 Then

                rgv_removals.DataSource = removalsDT

                For i = 0 To rgv_removals.Columns.Count - 1

                    rgv_removals.Columns(i).TextAlignment = ContentAlignment.MiddleCenter
                    rgv_removals.Columns(i).BestFit()
                    rgv_removals.Columns(i).Width += 50
                Next

                btn_remCheck.Enabled = True
            Else

                btn_remCheck.Enabled = False
                rgv_removals.DataSource = ""
            End If
        Else

            MsgBox(removalsErrorMsg.Trim + vbCr + "Contact IT", MsgBoxStyle.Exclamation, "Error")
        End If

        img_removals.Visible = False
    End Sub

    Private Sub bgw_databaseLoader_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgw_databaseLoader.DoWork

        Try

            Dim availDS As DataSet = getDataSetFromPTLSAGE("StockChecker", db_conx(My.Settings.connectionString), "@bothDB", both)

            If availDS.Tables(0).Rows.Count > 0 Then

                ' Dim imageDS As DataSet = getDataSetFromPTLSAGECustomQuery("SELECT * FROM PinnPriceCheck.dbo.ImageTable WITH(NOLOCK)", db_conx(My.Settings.connectionString))

                ' Dim oldDS As DataSet = New DataSet()

                ' If both Then

                'oldDS = getDataSetFromPTLSAGECustomQuery("SELECT * FROM PinnPriceCheck.dbo.pictures WITH(NOLOCK)", db_conx(My.Settings.connectionString))
                'End If

                'If imageDS.Tables(0).Rows.Count > 0 Then
                'databaseDT.Clear()
                'databaseDT.Columns.Clear()

                'databaseDT.Columns.Add("Product")
                'databaseDT.Columns.Add("Description")
                'databaseDT.Columns.Add("Physical")
                'databaseDT.Columns.Add("On Order")
                'databaseDT.Columns.Add("Available")
                'databaseDT.Columns.Add("Bin Number")
                'databaseDT.Columns.Add("Analysis A")

                ' Dim prodList As List(Of String) = New List(Of String)
                ' Dim oldList As List(Of String) = New List(Of String)

                'For l = 0 To imageDS.Tables(0).Rows.Count - 1

                'prodList.Add(imageDS.Tables(0).Rows(l)(1).ToString.Trim)
                ' Next

                'If both Then

                'For l = 0 To oldDS.Tables(0).Rows.Count - 1

                'oldList.Add(oldDS.Tables(0).Rows(l)(0).ToString.Trim)
                'Next
                'End If

                ' For i = 0 To availDS.Tables(0).Rows.Count - 1

                ' Dim found As Boolean = False

                '  If prodList.Contains(availDS.Tables(0).Rows(i)(0).ToString.Trim) Then

                '   found = True
                '    Continue For
                ' End If

                ' If (both And found = False) Then

                ' If oldList.Contains(availDS.Tables(0).Rows(i)(0).ToString.Trim) Then

                '    found = True
                '    Continue For
                '  End If
                'End If

                '  If found = False Then

                '     Dim dr As DataRow = availDS.Tables(0).Rows(i)
                '      databaseDT.ImportRow(dr)
                '    End If
                ' Next
                ' Else

                databaseDT = availDS.Tables(0)
                ' End If
            End If
        Catch ex As Exception

            databaseErrorMsg = ex.ToString.Trim
        End Try
    End Sub

    Private Sub bgw_databaseLoader_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgw_databaseLoader.RunWorkerCompleted

        If databaseErrorMsg = "" Then

            rgv_database.DataSource = databaseDT

            For i = 0 To rgv_database.Columns.Count - 1

                rgv_database.Columns(i).TextAlignment = ContentAlignment.MiddleCenter
            Next

            img_dbLoad.Visible = False
            chk_bothDB.Enabled = True
        Else

            MsgBox(databaseErrorMsg.Trim + vbCr + "Contact IT", MsgBoxStyle.Exclamation, "Error")
        End If
    End Sub

        Private Sub bgw_moveToWorks_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgw_moveToWorks.DoWork

            Try

                For i = 0 To additionsDT.Rows.Count - 1

                    Dim product As String = additionsDT.Rows(i)(0).ToString.Trim
                    Dim strings() As String = Directory.GetFiles(stockPath, "*" + product + "*")

                    If strings.Length <> 0 Then

                        For Each p As String In strings

                            Dim img As String = p.Replace(stockPath, worksPath).Trim

                            If File.Exists(img.Trim) Then

                                Try
                                    File.Delete(img.Trim)
                                Catch ex As Exception

                                End Try
                            End If

                            Try
                                File.Move(p.Trim, img.Trim)
                            Catch ex As Exception

                            End Try
                        Next
                    End If
                Next
            Catch ex As Exception

                errorMsg = ex.ToString.Trim
            End Try
        End Sub

        Private Sub bgw_moveToWorks_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgw_moveToWorks.RunWorkerCompleted

            If errorMsg.ToString.Trim = "" Then

                MsgBox("Successfully Moved Images!", MsgBoxStyle.Information, "SUCCESS!")
                refreshAdditions()
            Else
                MsgBox("Couldn't Move Images - Something Went Wrong" + vbCr + vbCr + errorMsg, MsgBoxStyle.Exclamation, "Error!")
            End If
        End Sub

        Private Sub bgw_moveToStock_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgw_moveToStock.DoWork

            For i = 0 To removalsDT.Rows.Count - 1

                Dim product As String = removalsDT.Rows(i)(0).ToString.Trim
                Dim strings() As String = Directory.GetFiles(worksPath, "*" + product + "*")

                If strings.Length <> 0 Then

                    For Each p As String In strings

                        Dim img As String = p.Replace(worksPath, stockPath).Trim

                        If File.Exists(img.Trim) Then

                            Try
                                File.Delete(img.Trim)
                            Catch ex As Exception

                            End Try
                        End If

                        Try
                            File.Move(p.Trim, img.Trim)
                        Catch ex As Exception

                        End Try
                    Next
                End If
            Next
        End Sub

        Private Sub bgw_moveToStock_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgw_moveToStock.RunWorkerCompleted

            If removalsErrorMsg.ToString.Trim = "" Then

                MsgBox("Successfully Moved Images!", MsgBoxStyle.Information, "SUCCESS!")
                refreshAdditions()
            Else
                MsgBox("Couldn't Move Images - Something Went Wrong" + vbCr + vbCr + errorMsg, MsgBoxStyle.Exclamation, "Error!")
            End If
        End Sub

        Private Sub bgw_cloudinaryLoader_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgw_cloudinaryLoader.DoWork

            Try
                Dim ds As DataSet = getDataSetFromPTLSAGE("CloudinaryImageChecker", db_conx(My.Settings.connectionString))

                If ds.Tables(0).Rows.Count > 0 Then

                    For i = 0 To ds.Tables(0).Rows.Count - 1

                        Dim prod As String = ds.Tables(0).Rows(i)(0).ToString.Trim
                        Dim prodImg As String = ds.Tables(0).Rows(i)(1).ToString.Trim

                        Dim g As New GetResourceParams(prodImg)
                        Dim gr As GetResourceResult = Cloudinary.GetResource(g)

                        Dim jsonString = gr.JsonObj.ToString

                        Dim ro As RootObject
                        ro = JsonConvert.DeserializeObject(Of RootObject)(jsonString)

                        If ro.format = "unknown" Then

                            Dim ds2 As DataSet = getDataSetFromPTLSAGE("StockChecker", db_conx(My.Settings.connectionString), "@prod", prod)

                            If ds2.Tables(0).Rows.Count > 0 Then

                                Dim dr As DataRow
                                dr = ds2.Tables(0).Rows(0)
                                cloudinaryDT.ImportRow(dr)
                            End If
                        End If
                    Next
                End If
            Catch ex As Exception

                cloudinaryErrorMsg = ex.ToString.Trim
            End Try
        End Sub

        Private Sub bgw_cloudinaryLoader_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgw_cloudinaryLoader.RunWorkerCompleted

            If cloudinaryErrorMsg = "" Then

                If cloudinaryDT.Rows.Count > 0 Then

                    rgv_cloudinary.DataSource = cloudinaryDT

                    For i = 0 To rgv_cloudinary.Columns.Count - 1

                        rgv_cloudinary.Columns(i).TextAlignment = ContentAlignment.MiddleCenter
                    Next
                Else

                    rgv_cloudinary.DataSource = ""
                End If
            Else

                rgv_cloudinary.DataSource = ""
                MsgBox(cloudinaryErrorMsg.Trim + vbCr + "CONTACT IT", MsgBoxStyle.Exclamation, "ERROR!")
            End If

            img_cloudinary.Visible = False
        End Sub
    End Class

    Public Class TopObject

    Public Property resources() As List(Of RootObject)
        Get
            Return m_resource
        End Get
        Set(value As List(Of RootObject))
            m_resource = value
        End Set
    End Property

    Private m_resource As List(Of RootObject)
End Class

Public Class RootObject
    Public Property public_id() As String
        Get
            Return m_public_id
        End Get
        Set(value As String)
            m_public_id = value
        End Set
    End Property
    Private m_public_id As String
    Public Property format() As String
        Get
            Return m_format
        End Get
        Set(value As String)
            m_format = value
        End Set
    End Property
    Private m_format As String
    Public Property version() As Integer
        Get
            Return m_version
        End Get
        Set(value As Integer)
            m_version = value
        End Set
    End Property
    Private m_version As Integer
    Public Property resource_type() As String
        Get
            Return m_resource_type
        End Get
        Set(value As String)
            m_resource_type = value
        End Set
    End Property
    Private m_resource_type As String
    Public Property type() As String
        Get
            Return m_type
        End Get
        Set(value As String)
            m_type = value
        End Set
    End Property
    Private m_type As String
    Public Property placeholder() As Boolean
        Get
            Return m_placeholder
        End Get
        Set(value As Boolean)
            m_placeholder = value
        End Set
    End Property
    Private m_placeholder As Boolean
    Public Property created_at() As String
        Get
            Return m_created_at
        End Get
        Set(value As String)
            m_created_at = value
        End Set
    End Property
    Private m_created_at As String
    Public Property url() As String
        Get
            Return m_url
        End Get
        Set(value As String)
            m_url = value
        End Set
    End Property
    Private m_url As String
    Public Property secure_url() As String
        Get
            Return m_secure_url
        End Get
        Set(value As String)
            m_secure_url = value
        End Set
    End Property
    Private m_secure_url As String
    Public Property next_cursor() As String
        Get
            Return m_next_cursor
        End Get
        Set(value As String)
            m_next_cursor = value
        End Set
    End Property
    Private m_next_cursor As String
    Public Property derived() As List(Of Derived)
        Get
            Return m_derived
        End Get
        Set(value As List(Of Derived))
            m_derived = value
        End Set
    End Property
    Private m_derived As List(Of Derived)
End Class
