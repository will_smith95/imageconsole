﻿Imports System.Data.SqlClient

Module WSModule

#Region "----------------- DATABASE FUNCTIONS ------------------"

    Public Function db_conx(ByVal hash As String)
        Dim returnValue As String = ""
        If hash = "034cdaf241d87a07c5e5d47261a6f2de" Then
            returnValue = "Data Source=PTLSAGE.pricecheck.co.uk;Initial Catalog=MPSPriceCheck;User ID=admin;Password=barry250372;"
            Return returnValue
        End If
        Return returnValue
    End Function

    Public Function sanitize(ByVal s As String)

        s = s.Replace(".jpg", "")
        s = s.Replace(".jpeg", "")
        s = s.Replace(".JPEG", "")
        s = s.Replace(".JPG", "")
        s = s.Replace(".png", "")
        Return s
    End Function

    Function getDataSetFromPTLSAGECustomQuery(ByVal query As String, _
                                   ByVal connectionString As String, _
                                   Optional param1Name As String = "", _
                                   Optional param1Val As String = "", _
                                   Optional param2Name As String = "", _
                                   Optional param2Val As String = "", _
                                   Optional param3Name As String = "", _
                                   Optional param3Val As String = "", _
                                   Optional param4Name As String = "", _
                                   Optional param4Val As String = "")

        Dim tempDataset As New DataSet
        Dim command As New SqlCommand
        Dim connection As SqlConnection
        command.Parameters.Clear()
        connection = New SqlConnection(connectionString)
        Try
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.Text
            command.CommandText = query
            If param1Name <> "" And param1Val <> "" Then
                command.Parameters.AddWithValue(param1Name, param1Val)
            End If
            If param2Name <> "" And param2Val <> "" Then
                command.Parameters.AddWithValue(param2Name, param2Val)
            End If
            If param3Name <> "" And param3Val <> "" Then
                command.Parameters.AddWithValue(param3Name, param3Val)
            End If
            If param4Name <> "" And param4Val <> "" Then
                command.Parameters.AddWithValue(param4Name, param4Val)
            End If
            Dim adapter As New SqlDataAdapter(command)
            adapter.Fill(tempDataset)
            connection.Close()
        Catch ex As System.Exception
            tempDataset.Tables.Add()
            tempDataset.Tables(0).Rows.Add()
            tempDataset.Tables(0).Columns.Add()

            tempDataset.Tables(0).Rows(0).Item(0) = ex.ToString
            Console.WriteLine(ex.ToString)
            connection.Close()
        End Try

        Return tempDataset

    End Function

    Function getDataSetFromPTLSAGE(ByVal storedProcedure As String, _
                                   ByVal connectionString As String, _
                                   Optional param1Name As String = "", _
                                   Optional param1Val As String = "", _
                                   Optional param2Name As String = "", _
                                   Optional param2Val As String = "", _
                                   Optional param3Name As String = "", _
                                   Optional param3Val As String = "", _
                                   Optional param4Name As String = "", _
                                   Optional param4Val As String = "", _
                                   Optional param5Name As String = "", _
                                   Optional param5Val As String = "", _
                                   Optional param6Name As String = "", _
                                   Optional param6Val As String = "")

        Dim tempDataset As New DataSet
        Dim command As New SqlCommand
        Dim connection As SqlConnection
        command.Parameters.Clear()
        connection = New SqlConnection(connectionString)
        Try
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = storedProcedure
            If param1Name <> "" And param1Val <> "" Then
                command.Parameters.AddWithValue(param1Name, param1Val)
            End If
            If param2Name <> "" And param2Val <> "" Then
                command.Parameters.AddWithValue(param2Name, param2Val)
            End If
            If param3Name <> "" And param3Val <> "" Then
                command.Parameters.AddWithValue(param3Name, param3Val)
            End If
            If param4Name <> "" And param4Val <> "" Then
                command.Parameters.AddWithValue(param4Name, param4Val)
            End If
            If param5Name <> "" And param5Val <> "" Then
                command.Parameters.AddWithValue(param5Name, param5Val)
            End If
            If param6Name <> "" And param6Val <> "" Then
                command.Parameters.AddWithValue(param6Name, param6Val)
            End If
            Dim adapter As New SqlDataAdapter(command)
            adapter.Fill(tempDataset)
            connection.Close()
        Catch ex As System.Exception
            tempDataset.Tables.Add()
            tempDataset.Tables(0).Rows.Add()
            tempDataset.Tables(0).Columns.Add()

            tempDataset.Tables(0).Rows(0).Item(0) = ex.ToString
            Console.WriteLine(ex.ToString)
            connection.Close()
        End Try

        Return tempDataset

    End Function

    Function executeNonQueryFromPTLSAGECustomQuery(ByVal query As String, _
                                       ByVal connectionString As String, _
                                       Optional param1Name As String = "", _
                                       Optional param1Val As String = "", _
                                       Optional param2Name As String = "", _
                                       Optional param2Val As String = "", _
                                       Optional param3Name As String = "", _
                                       Optional param3Val As String = "", _
                                       Optional param4Name As String = "", _
                                       Optional param4Val As String = "", _
                                       Optional param5Name As String = "", _
                                       Optional param5Val As String = "", _
                                       Optional param6Name As String = "", _
                                       Optional param6Val As String = "", _
                                       Optional param7Name As String = "", _
                                       Optional param7Val As String = "", _
                                       Optional param7Type As String = "String")
        Dim command As New SqlCommand
        Dim connection As SqlConnection
        command.Parameters.Clear()
        connection = New SqlConnection(connectionString)
        Try
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.Text
            command.CommandText = query
            If param1Name <> "" And param1Val <> "" Then
                command.Parameters.AddWithValue(param1Name, param1Val)
            End If
            If param2Name <> "" And param2Val <> "" Then
                command.Parameters.AddWithValue(param2Name, param2Val)
            End If
            If param3Name <> "" And param3Val <> "" Then
                command.Parameters.AddWithValue(param3Name, param3Val)
            End If
            If param4Name <> "" And param4Val <> "" Then
                command.Parameters.AddWithValue(param4Name, param4Val)
            End If
            If param5Name <> "" And param5Val <> "" Then
                command.Parameters.AddWithValue(param5Name, param5Val)
            End If
            If param6Name <> "" And param6Val <> "" Then
                command.Parameters.AddWithValue(param6Name, param6Val)
            End If
            If param7Name <> "" And param7Val <> "" Then
                If param7Type = "Date" Then
                    command.Parameters.AddWithValue(param7Name, CDate(param7Val))
                End If

            End If
            command.ExecuteNonQuery()
            connection.Close()
        Catch ex As System.Exception
            Console.WriteLine(ex.ToString)
            connection.Close()
            Return False
        End Try
        Return True
    End Function
#End Region
End Module
